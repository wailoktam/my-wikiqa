# Finding Prototypes of Answers for Improving Answer Sentence Selection 

This repository is created for storing scripts actually doing the replacement of wh-words in WikiQA data by prototypes of answers mentioned in Tam et al.: Finding Prototypes of Answers for Improving Answer Sentence Selection, IJCAI 2017. Transformed data and untransformed data used in our experiment are also included.

## Purposes of scripts written from scratch

* sharWikiQTfull.py
    * extract data from WikiQA dataset and pickle them

* sharWikiQTctrl.py
    * split the output of  sharWikiQTfull.py by question types

* sharWikiQT.py
    * replace the question words in output of sharWikiQTctrl.py by prototypes